# Data Programming Project

Name of netlify website: hopeful-edison-30dadd
Link to netlify: https://hopeful-edison-30dadd.netlify.app/

Instructions for the final project
Uroš Godnov and Peter Muršič
May 2020

Due date is 14.6.2020 23:59. There will not be any oral defenses of the projects.

The topic for your project must be on university level, topic is up to you. 
Pick a dataset among https://www.kaggle.com/datasets or similar database and study it. 

Use only methods, that lead to a solution, like user defined functions, mutate, summarise, ggplot, …

The project should include the following sections:

1) Introduction
2) Presentation and description of the problem
3) Presentation of the data
4) Exploratory data analysis, a correlation woulod be splendid, but it is not necessary.
5) Visualisation of the data, at least 3 relevant ggplots
6) Interpretation / Conclusion
7) References

The project must be published online https://www.netlify.com/. 

Aside from this you must also submit it via e-classroom. 
Submit the .rmd file, .html file and .txt file with the name of the website, where it is published. 
If you do not submit all of the required files its and automatic fail.